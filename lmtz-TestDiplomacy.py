# -------
# imports
# -------

from io import StringIO
from unittest import main, TestCase

from Diplomacy import diplomacy_read, diplomacy_eval, diplomacy_print, diplomacy_solve

# -----------
# TestDiplomacy
# -----------


class TestDiplomacy (TestCase):
    # ----
    # read
    # ----

    def test_read(self):
        s = "A Madrid Hold\n"
        line = diplomacy_read(s)
        self.assertEqual(line,  ["A", "Madrid", "Hold"])
        
    def test_read2(self):
        s = "A Austin Hold\nB Barcelona Support A\nC Madrid Move Austin\n"
        line = diplomacy_read(s)
        self.assertEqual(line,  ["A", "Austin", "Hold", "B", "Barcelona", "Support", "A", "C", "Madrid", "Move", "Austin"])

    def test_read3(self):
        s = "A London Support B\nB Madrid Support A\nC Austin Move London\nD Paris Hold\n"
        line = diplomacy_read(s)
        self.assertEqual(line,  ["A", "London", "Support", "B", "B", "Madrid", "Support", "A", "C", "Austin", "Move", "London", "D", "Paris", "Hold"])

    def test_read4(self):
        s = "A Paris Hold\nB London Move Tokyo\nC Tokyo Move Paris\nD Madrid Support A\n"
        line = diplomacy_read(s)
        self.assertEqual(line,  ["A", "Paris", "Hold", "B", "London", "Move", "Tokyo", "C", "Tokyo", "Move", "Paris", "D", "Madrid", "Support", "A"])
    

    # ----
    # eval
    # ----

    def test_eval_1(self):
        v = diplomacy_eval(["A", "Madrid", "Hold"])
        self.assertEqual(v, ["A Madrid"])

    def test_eval_2(self):
        v = diplomacy_eval(["A", "Madrid", "Hold", "B", "Barcelona", "Move", "London", "C", "London", "Support", "A"])
        self.assertEqual(v, ["A Madrid", "B [dead]", "C [dead]"])

    def test_eval_3(self):
        v = diplomacy_eval(["A", "London", "Hold", "B", "Paris", "Move", "London"])
        self.assertEqual(v, ["A [dead]", "B [dead]"])

    def test_eval_4(self):
        v = diplomacy_eval(["A", "Austin", "Hold", "B", "Barcelona", "Support", "A", "C", "Tokyo", "Move", "Barcelona"])
        self.assertEqual(v, ["A Austin", "B [dead]", "C [dead]"])

    # -----
    # print
    # -----

    def test_print(self):
        w = StringIO()
        diplomacy_print(w, "A", "Madrid")
        self.assertEqual(w.getvalue(), "A Madrid\n")
        
    def test_print2(self):
        w = StringIO()
        diplomacy_print(w, "B", "London")
        self.assertEqual(w.getvalue(), "B London\n")
        
    def test_print3(self):
        w = StringIO()
        diplomacy_print(w, "A", "[dead]")
        self.assertEqual(w.getvalue(), "A [dead]\n")
        
    def test_print4(self):
        w = StringIO()
        diplomacy_print(w, "B", "Tokyo")
        self.assertEqual(w.getvalue(), "B Tokyo\n")

    # -----
    # solve
    # -----

    def test_solve(self):
        r = StringIO("A Madrid Hold\n")
        w = StringIO()
        diplomacy_solve(r, w)
        self.assertEqual(
            w.getvalue(), "A Madrid\n")
            
    def test_solve2(self):
        r = StringIO("A Paris Hold\nB Austin Support A\nC Madrid Move Austin\n")
        w = StringIO()
        diplomacy_solve(r, w)
        self.assertEqual(
            w.getvalue(), "A Paris\nB [dead]\nC [dead]\n")
            
    def test_solve3(self):
        r = StringIO("A Austin Hold\nB Barcelona Move Tokyo\nC Tokyo Move Austin\n")
        w = StringIO()
        diplomacy_solve(r, w)
        self.assertEqual(
            w.getvalue(), "A [dead]\nB Tokyo\nC [dead]\n")
            
    def test_solve4(self):
        r = StringIO("A Barcelona Support B\nB Tokyo Move Paris\nC Paris Hold\n")
        w = StringIO()
        diplomacy_solve(r, w)
        self.assertEqual(
            w.getvalue(), "A Barcelona\nB Paris\nC [dead]\n")

# ----
# main
# ----

if __name__ == "__main__":
    main()

""" #pragma: no cover
$ coverage run --branch TestDiplomacy.py >  TestDiplomacy.out 2>&1


$ cat Diplomacy.out
.......
----------------------------------------------------------------------
Ran 7 tests in 0.000s
OK


$ coverage report -m                   >> TestDiplomacy.out



$ cat TestDiplomacy.out
.......
----------------------------------------------------------------------
Ran 7 tests in 0.000s
OK
Name             Stmts   Miss Branch BrPart  Cover   Missing
------------------------------------------------------------
Diplomacy.py          12      0      2      0   100%
TestDiplomacy.py      32      0      0      0   100%
------------------------------------------------------------
TOTAL               44      0      2      0   100%
"""

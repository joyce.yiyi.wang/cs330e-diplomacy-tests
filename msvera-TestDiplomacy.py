from io import StringIO
from unittest import main, TestCase

from Diplomacy import diplomacy_eval, diplomacy_print, diplomacy_solve

# -----------
# TestDiplomacy
# -----------


class TestDiplomacy (TestCase):
    # ----
    # read
    # ----

    """def test_eval_1(self):
        s = "A Madrid Hold"
        str_list = diplomacy_eval(s)
        self.assertEqual(str_list,  {"A":["Madrid", 0, "Alive"]})
    # ----
    # eval
    # ----

    def test_eval_2(self):
        v = diplomacy_eval("A Madrid Hold")

        self.assertEqual(v, {"A":["Madrid", 0, "Alive"]})

    # -----
    # print
    # -----

    def test_print_1(self):
        w = StringIO()
        diplomacy_print({"A":["Madrid", 0, "Alive"]}, w)
        self.assertEqual(w.getvalue(), "A Madrid\n")"""


    # -----
    # solve
    # -----

    def test_solve_1(self):
        r = StringIO("A Madrid Hold\nB London Move Madrid\nC Austin Move Madrid\n")
        w = StringIO()
        diplomacy_solve(r, w)
        self.assertEqual(w.getvalue(), "A [dead]\nB [dead]\nC [dead]\n")

    def test_solve_2(self):
        r = StringIO("A Madrid Hold\nB London Move Madrid\nC Austin Support A\nD NewYork Support B\n")
        w = StringIO()
        diplomacy_solve(r, w)
        self.assertEqual(w.getvalue(), "A [dead]\nB [dead]\nC Austin\nD NewYork\n")

    def test_solve_3(self):
        r = StringIO("A Madrid Hold\nB London Move Madrid\nC Austin Support A\nD NewYork Hold\n")
        w = StringIO()
        diplomacy_solve(r, w)
        self.assertEqual(w.getvalue(), "A Madrid\nB [dead]\nC Austin\nD NewYork\n")

    def test_solve_4(self):
        r = StringIO("A Madrid Hold\nB London Move Madrid\nC Austin Support A\nD NewYork Move Austin\nE MexicoCity Support D")
        w = StringIO()
        diplomacy_solve(r, w)
        self.assertEqual(w.getvalue(), "A [dead]\nB [dead]\nC [dead]\nD Austin\nE MexicoCity\n")

    def test_solve_5(self):
        r = StringIO("A Madrid Hold\nB London Support C\nC Austin Move Madrid\n")
        w = StringIO()
        diplomacy_solve(r, w)
        self.assertEqual(w.getvalue(), "A [dead]\nB London\nC Madrid\n")


# ----
# main
# ----


if __name__ == "__main__":  # pragma: no cover
    main()

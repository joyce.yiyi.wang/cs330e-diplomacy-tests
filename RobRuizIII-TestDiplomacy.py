from io import StringIO
from unittest import main, TestCase

from Diplomacy import Army, diplomacy_read, make_support_hold, support_count_army, battle_for_city, diplomacy_evaluate, diplomacy_print, diplomacy_solve


class TestDiplomacy(TestCase):

    # ----
    # read
    # ----

    def test_read_1(self):
        s = "A Austin Move Houston\n"
        split_s = diplomacy_read(s)
        self.assertEqual(split_s,['A','Austin','Move','Houston'])
    
    def test_read_2(self):
        s = "B Barcelona Hold\n"
        split_s = diplomacy_read(s)
        self.assertEqual(split_s,['B','Barcelona','Hold'])

    def test_read_3(self):
        s = "Z Denver Support A\n"
        split_s = diplomacy_read(s)
        self.assertEqual(split_s,['Z','Denver','Support','A'])

    
    # ----
    # Army Class Tests
    # ----

    # Init test

    def test_init_1(self):
        army_object = Army('A', 'Austin', 'Hold')
        self.assertEqual(army_object.letter, 'A')
        self.assertEqual(army_object.city, 'Austin')
        self.assertEqual(army_object.action, 'Hold')
        self.assertEqual(army_object.towards, None)
        self.assertEqual(army_object.dead, False)
        self.assertEqual(army_object.support, 1)

    def test_init_2(self):
        army_object = Army('B', 'NewYork', ['Move','Rome'])
        self.assertEqual(army_object.letter, 'B')
        self.assertEqual(army_object.city, 'NewYork')
        self.assertEqual(army_object.action, 'Move')
        self.assertEqual(army_object.towards, 'Rome')
        self.assertEqual(army_object.dead, False)
        self.assertEqual(army_object.support, 1)

    def test_init_3(self):
        army_object = Army('M', 'ElPaso', ['Support','B'])
        self.assertEqual(army_object.letter, 'M')
        self.assertEqual(army_object.city, 'ElPaso')
        self.assertEqual(army_object.action, 'Support')
        self.assertEqual(army_object.towards, 'B')
        self.assertEqual(army_object.dead, False)
        self.assertEqual(army_object.support, 1)


    # Army.add_support()

    def test_add_support_1(self):
        army_object = Army('L', 'Guadalajara', 'Hold')
        army_object.add_support()
        self.assertEqual(army_object.support, 2)

    def test_add_support_2(self):
        army_object = Army('N', 'Monterrey', ['Move','Cancun'])
        army_object.support = 2
        army_object.add_support()
        self.assertEqual(army_object.support, 3)

    def test_add_support_3(self):
        army_object = Army('C', 'LasCruces', ['Support','N'])
        army_object.add_support()
        self.assertEqual(army_object.support, 2)

    
    # Army.support_2_hold()

    def test_support_2_hold_1(self):
        army_object = Army('E', 'Dallas', ['Support','M'])
        army_object.support_2_hold()
        self.assertEqual(army_object.action, 'Hold')
        self.assertEqual(army_object.towards, None)

    def test_support_2_hold_2(self):
        army_object = Army('X', 'Austin', ['Support','C'])
        army_object.support_2_hold()
        self.assertEqual(army_object.action, 'Hold')
        self.assertEqual(army_object.towards, None)

    def test_support_2_hold_3(self):
        army_object = Army('H', 'Madrid', ['Support','A'])
        army_object.support_2_hold()
        self.assertEqual(army_object.action, 'Hold')
        self.assertEqual(army_object.towards, None)


    # ----
    # make_support_hold
    # ----

    def test_make_support_hold_1(self):
        support_armies = [Army('M', 'Austin', ['Support','A']),
                            Army('A', 'Houston', ['Support','C']),
                            Army('H', 'Madrid', ['Support','A'])]
        move_armies = [Army('N', 'Barcelona', ['Move','Madrid']), 
                        Army('L', 'Rome', ['Move','Austin']),
                        Army('B', 'Cancun', ['Move','Houston'])]
        make_support_hold(support_armies, move_armies)

        for army_object in support_armies:
            self.assertEqual(army_object.action, 'Hold')
            self.assertEqual(army_object.towards, None)


    def test_make_support_hold_2(self):
        support_armies = [Army('Z', 'Austin', ['Support','B'])]
        move_armies = [Army('N', 'Barcelona', ['Move','Madrid']), 
                        Army('L', 'Rome', ['Move','Austin']),
                        Army('B', 'Cancun', ['Move','Houston'])]
        make_support_hold(support_armies, move_armies)
        
        for army_object in support_armies:
            self.assertEqual(army_object.action, 'Hold')
            self.assertEqual(army_object.towards, None)


    def test_make_support_hold_3(self):
        support_armies = [Army('A', 'Denver', ['Support','A']),
                            Army('B', 'Rome', ['Support','C'])]
        move_armies = [Army('C', 'Barcelona', ['Move','Rome']), 
                        Army('D', 'Madrid', ['Move','Denver'])]

        make_support_hold(support_armies, move_armies)
        
        for army_object in support_armies:
            self.assertEqual(army_object.action, 'Hold')
            self.assertEqual(army_object.towards, None)

    # ----
    # support_count_army
    # ----


    def test_support_count_army_1(self):
        battle_armies = [Army('B', 'NewYork', ['Move','Rome']), 
                            Army('A', 'Austin', 'Hold'), 
                            Army('D', 'Madrid', ['Move','Denver'])]

        support_armies = [Army('L', 'Houston', ['Support','B']), 
                            Army('N', 'Guadalajara', ['Support','A']), 
                            Army('K', 'KansasCity', ['Support','D'])]

        support_count_army(battle_armies, support_armies)

        for army_object in battle_armies:
            self.assertEqual(army_object.support, 2)



    def test_support_count_army_2(self):
        battle_armies = [Army('G', 'SanAntonio', ['Move','Austin'])]

        support_armies = [Army('L', 'Houston', ['Support','G']), 
                            Army('N', 'Guadalajara', ['Support','G']), 
                            Army('K', 'KansasCity', ['Support','G'])]

        support_count_army(battle_armies, support_armies)

        for army_object in battle_armies:
            self.assertEqual(army_object.support, 4)


    
    def test_support_count_army_3(self):
        battle_armies = [Army('B', 'NewYork','Hold')]

        support_armies = [Army('P', 'Pittsburgh', ['Support','B']), 
                            Army('X', 'Baltimore', ['Support','B']), 
                            Army('Y', 'Cleveland', ['Support','B'])]

        support_count_army(battle_armies, support_armies)

        for army_object in battle_armies:
            self.assertEqual(army_object.support, 4)


    # ----
    # battle_for_city
    # ----

    def test_battle_for_city_1(self):
        hold_army = Army('B', 'NewYork','Hold')
        move_armies = [Army('N', 'Barcelona', ['Move','NewYork']), 
                        Army('L', 'Rome', ['Move','NewYork']),
                        Army('B', 'Cancun', ['Move','Houston'])]

        armies_in_city = battle_for_city(hold_army, move_armies)

        for army in armies_in_city:
            if army.towards == None:
                self.assertEqual(army.city, 'NewYork')
            else:
                self.assertEqual(army.towards, 'NewYork')


    def test_battle_for_city_2(self):
        hold_army = Army('N', 'Barcelona','Hold')
        move_armies = [Army('O', 'Dallas', ['Move','NewYork']), 
                        Army('R', 'KansasCity', ['Move','Denver']),
                        Army('T', 'ElPaso', ['Move','Austin'])]

        armies_in_city = battle_for_city(hold_army, move_armies)

        for army in armies_in_city:
            if army.towards == None:
                self.assertEqual(army.city, 'Barcelona')
            else:
                self.assertEqual(army.towards, 'Barcelona')

    
    def test_battle_for_city_3(self):
        hold_army = Army('M', 'Austin','Hold')
        move_armies = [Army('B', 'Rome', ['Move','NewYork']), 
                        Army('E', 'Valencia', ['Move','Denver']),
                        Army('W', 'Houston', ['Move','Barcelona'])]

        armies_in_city = battle_for_city(hold_army, move_armies)

        for army in armies_in_city:
            if army.towards == None:
                self.assertEqual(army.city, 'Austin')
            else:
                self.assertEqual(army.towards, 'Austin')
    
    # ----
    # diplomacy_evaluate
    # ----

    def test_diplomacy_evaluate_1(self):
        armies = [['A','Madrid','Support','C'],
                    ['B','Barcelona','Move','Madrid'],
                    ['C','Houston','Move','Austin'],
                    ['D','Austin','Support', 'B']]

        armies_solved = diplomacy_evaluate(armies)

        self.assertEqual(armies_solved[0].letter, 'A')
        self.assertEqual(armies_solved[0].dead, True)

        self.assertEqual(armies_solved[1].letter, 'B')
        self.assertEqual(armies_solved[1].dead, True)

        self.assertEqual(armies_solved[2].letter, 'C')
        self.assertEqual(armies_solved[2].dead, True)

        self.assertEqual(armies_solved[3].letter, 'D')
        self.assertEqual(armies_solved[3].dead, True)


    def test_diplomacy_evaluate_2(self):
        armies = [['C','SanAntonio','Support','B'],
                    ['B','KansasCity','Move','Madrid'],
                    ['A','NewYork','Move','NewJersey']]

        armies_solved = diplomacy_evaluate(armies)

        self.assertEqual(armies_solved[0].letter, 'A')
        self.assertEqual(armies_solved[0].dead, False)

        self.assertEqual(armies_solved[1].letter, 'B')
        self.assertEqual(armies_solved[1].dead, False)

        self.assertEqual(armies_solved[2].letter, 'C')
        self.assertEqual(armies_solved[2].dead, False)



    def test_diplomacy_evaluate_3(self):
        armies = [['A','Madrid','Hold'],
                    ['B','Barcelona','Move','Madrid'],
                    ['C','NewYork','Support','B']]

        armies_solved = diplomacy_evaluate(armies)

        self.assertEqual(armies_solved[0].letter, 'A')
        self.assertEqual(armies_solved[0].dead, True)

        self.assertEqual(armies_solved[1].letter, 'B')
        self.assertEqual(armies_solved[1].dead, False)

        self.assertEqual(armies_solved[2].letter, 'C')
        self.assertEqual(armies_solved[2].dead, False)


    # ----
    # diplomacy_print
    # ----

    def test_diplomacy_print_1(self):
        w = StringIO()
        diplomacy_print(w,'A','[dead]')
        self.assertEqual(w.getvalue(),'A [dead]\n')

    def test_diplomacy_print_2(self):
        w = StringIO()
        diplomacy_print(w,'B','Barcelona')
        self.assertEqual(w.getvalue(),'B Barcelona\n')
    
    def test_diplomacy_print_3(self):
        w = StringIO()
        diplomacy_print(w,'C','NewYork')
        self.assertEqual(w.getvalue(),'C NewYork\n')

    
    # ----
    # diplomacy_solve
    # ----


    def test_diplomacy_solve_1(self):
        r = StringIO("A Madrid Hold\nB Barcelona Move Madrid\nC NewYork Support B\n")
        w = StringIO()
        diplomacy_solve(r, w)
        self.assertEqual(
            w.getvalue(), "A [dead]\nB Madrid\nC NewYork\n")

    def test_diplomacy_solve_2(self):
        r = StringIO("C SanAntonio Support B\nB KansasCity Move Madrid\nA Madrid Hold\n")
        w = StringIO()
        diplomacy_solve(r, w)
        self.assertEqual(
            w.getvalue(), "A [dead]\nB Madrid\nC SanAntonio\n")

    def test_diplomacy_solve_3(self):
        r = StringIO("A Houston Support C\nB Austin Move Houston\nC LasCruces Move ElPaso\nD ElPaso Support B\n")
        w = StringIO()
        diplomacy_solve(r, w)
        self.assertEqual(
            w.getvalue(), "A [dead]\nB [dead]\nC [dead]\nD [dead]\n")



if __name__ == "__main__":
    main()   
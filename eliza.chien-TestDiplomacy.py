#!/usr/bin/env python3

# -------
# imports
# -------

from io import StringIO
from unittest import main, TestCase

from Diplomacy import diplomacy_recursive, diplomacy_read, diplomacy_format, diplomacy_print, diplomacy_solve

# -----------
# TestDiplomacy
# -----------

class TestDiplomacy(TestCase):
    # ---------
    # recursive
    # ---------
    
    def test_recur_1(self):
        army = 'A'
        armies = {'A': ['Paris', 'alive', {'B'}], 'B': ['London', 'dead', set()]}
        cities = {'London': {'B'}, 'Paris': {'A'}}
        diplomacy_recursive(army, armies, cities)
        self.assertEqual(armies, {'A': ['Paris', 'alive', {'B'}], 'B': ['London', 'dead', set()]})
        self.assertEqual(cities, {'Paris': {'A'}, 'London': {'B'}})
        
    def test_recur_2(self):
        army = 'A'
        armies = {'A': ['Paris', 'alive', {'B'}], 'B': ['London', 'dead', set()]}
        cities = {'London': {'B'}, 'Paris': {'A'}}
        diplomacy_recursive(army, armies, cities)
        self.assertEqual(armies, {'A': ['Paris', 'alive', {'B'}], 'B': ['London', 'dead', set()]})
        self.assertEqual(cities, {'Paris': {'A'}, 'London': {'B'}})
    
    # ----
    # read
    # ----
    
    def test_read_1(self):
        s = "A Madrid hold\n"
        phrase = diplomacy_read(s)
        self.assertEqual(phrase, ['A', 'Madrid', 'hold'])
        
    def test_read_2(self):
        s = "A Madrid move Paris\n"
        phrase = diplomacy_read(s)
        self.assertEqual(phrase, ['A', 'Madrid', 'move', 'Paris'])
        
    # ------
    # format
    # ------
    
    def test_format_1(self):
        commands = {'A': ['Madrid', 'hold']}
        armies, cities = diplomacy_format(commands)
        self.assertEqual(armies, {'A': ['Madrid', 'unknown', set()]})
        self.assertEqual(cities, {'Madrid': {'A'}})
        
    def test_format_2(self):
        commands = {'A': ['Madrid', 'move', 'Paris'],
                    'B': ['Paris', 'move', 'Madrid']}
        armies, cities = diplomacy_format(commands)
        self.assertEqual(cities, {'Madrid': {'B'}, 'Paris': {'A'}})
        self.assertEqual(armies, {'A': ['Paris', 'unknown', set()], 'B': ['Madrid', 'unknown', set()]})
        
    def test_format_3(self):
        commands = {'A': ['Madrid', 'move', 'Paris'],
                    'B': ['London', 'support', 'A']}
        armies, cities = diplomacy_format(commands)
        self.assertEqual(cities, {'Madrid': set(), 'Paris': {'A'}, 'London': {'B'}})
        self.assertEqual(armies, {'A': ['Paris', 'unknown', {'B'}], 'B': ['London', 'unknown', set()]})
        
    # -----
    # print
    # -----
    
    def test_print_1(self):
        armies = {'A': ['Paris', 'dead', set()], 'B': ['London', 'alive', set()], 'C': ['Paris', 'alive', set()]}
        w = StringIO()
        diplomacy_print(w, armies)
        self.assertEqual(w.getvalue(), 'A [dead]\nB London\nC Paris\n')
    
    # -----
    # solve
    # -----
    
    def test_solve_1(self):
        r  = StringIO("A Madrid hold\n")
        w = StringIO()
        diplomacy_solve(r,w)
        self.assertEqual(w.getvalue(), "A Madrid\n")
        
    def test_solve_2(self):
        r = StringIO("A Madrid move Paris\n")
        w = StringIO()
        diplomacy_solve(r,w)
        self.assertEqual(w.getvalue(), "A Paris\n")
    
    def test_solve_3(self):
        r = StringIO("A Madrid support B\nB Paris hold")
        w = StringIO()
        diplomacy_solve(r,w)
        self.assertEqual(w.getvalue(), "A Madrid\nB Paris\n")
    
    def test_solve_4(self):
        r = StringIO("")
        w = StringIO()
        diplomacy_solve(r,w)
        self.assertEqual(w.getvalue(), "")
    
    def test_solve_5(self):
        r = StringIO('A Madrid hold\nB Paris move Madrid\n')
        w = StringIO()
        diplomacy_solve(r,w)
        self.assertEqual(w.getvalue(), 'A [dead]\nB [dead]\n')
    
    def test_solve_6(self):
        r = StringIO('A Madrid hold\nB Paris move Madrid\nC NewYork support A\n')
        w = StringIO()
        diplomacy_solve(r,w)
        self.assertEqual(w.getvalue(), 'A Madrid\nB [dead]\nC NewYork\n')
                                    
    def test_solve_7(self):
        r = StringIO('A Madrid hold\nB Paris move Madrid\nC NewYork move Madrid\nD Berlin support C\n')
        w = StringIO()
        diplomacy_solve(r,w)
        self.assertEqual(w.getvalue(), 'A [dead]\nB [dead]\nC Madrid\nD Berlin\n')
                                    
    def test_solve_8(self):
        r = StringIO('A Madrid hold\nB Paris move Madrid\nC NewYork support A\nD Berlin support B\n')
        w = StringIO()
        diplomacy_solve(r,w)
        self.assertEqual(w.getvalue(), 'A [dead]\nB [dead]\nC NewYork\nD Berlin\n')

    def test_solve_9(self):
        r = StringIO('A Madrid hold\nB Paris move Madrid\nC NewYork support A\nD Berlin move NewYork\n')
        w = StringIO()
        diplomacy_solve(r,w)
        self.assertEqual(w.getvalue(), 'A [dead]\nB [dead]\nC [dead]\nD [dead]\n')        

if __name__ == "__main__":
    main()                                    
    
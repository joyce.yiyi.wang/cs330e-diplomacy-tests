# 3 unit tests for Diplomacy_solve()

from io import StringIO
from unittest import main, TestCase

from Diplomacy import diplomacy_solve

class TestDiplomacy (TestCase):
	
	def test1(self): 
		w = StringIO()
		inp = StringIO('A Frankfurt Hold')
		diplomacy_solve(inp, w)
		check = 'A Frankfurt'
		self.assertEqual(w.getvalue(), check)
	
	def test2(self):
		w = StringIO()
		inp = StringIO("A Frankfurt Hold\nB Dublin Move Frankfurt\nC Dubai Support B")
		diplomacy_solve(inp, w)
		check = "A [dead]\nB Frankfurt\nC Dubai"
		self.assertEqual(w.getvalue(), check)
	
	def test3(self):
		w = StringIO()
		inp = StringIO("A Frankfurt Hold\nB Dublin Move Frankfurt")
		diplomacy_solve(inp, w)
		check = "A [dead]\nB [dead]"
		self.assertEqual(w.getvalue(), check)
	
	def test4(self):
		w = StringIO()
		inp = StringIO("A Frankfurt Hold\nB Dublin Move Frankfurt\nC Dubai Support B\nD Austin Move Dubai")
		diplomacy_solve(inp, w)
		check = "A [dead]\nB [dead]\nC [dead]\nD [dead]"
		self.assertEqual(w.getvalue(), check)
	
	def test5(self):
		w = StringIO()
		inp = StringIO("A Frankfurt Hold\nB Dublin Move Frankfurt\nC Dubai Move Frankfurt\nD Paris Support B")
		diplomacy_solve(inp, w)
		check = "A [dead]\nB Frankfurt\nC [dead]\nD Paris"
		self.assertEqual(w.getvalue(), check)
	
	def test6(self):
		w = StringIO()
		inp = StringIO("A Frankfurt Hold\nB Dublin Move Frankfurt\nC Dubai Move Frankfurt\nD Paris Support B\nE Austin Support A")
		diplomacy_solve(inp, w)
		check = "A [dead]\nB [dead]\nC [dead]\nD Paris\nE Austin"
		self.assertEqual(w.getvalue(), check)

# ----
# main
# ----
if __name__ == "__main__": #pragma: no cover
    main()

''' #pragma: no cover
$ coverage run --branch TestDiplomacy.py >  TestDiplomacy.out 2>&1

$ cat TestDiplomacy.out
......
----------------------------------------------------------------------
Ran 6 tests in 0.001s

OK

coverage report -m                   >> TestDiplomacy.out

$ cat TestDiplomacy.out
......
----------------------------------------------------------------------
Ran 6 tests in 0.001s

OK
Name               Stmts   Miss Branch BrPart  Cover   Missing
--------------------------------------------------------------
Diplomacy.py          63      0     44      2    98%   63->62, 83->82
TestDiplomacy.py      40      0      0      0   100%
--------------------------------------------------------------
TOTAL                103      0     44      2    99%

''' 
